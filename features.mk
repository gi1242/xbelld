# Edit as you will. You can also do
# 	env CFLAGS=... DEBUG=1 make
# instead of editing this file

# Set optimisations. (Overridden by the environment)
CFLAGS		?= -O2

# Enable debugging info.
#DEBUG 		:= 1

# Disable ALSA support
#WITHOUT_ALSA	:= 1

# Don't support WAVE files. Just emulate the PC Speaker.
#CFLAGS		+= -DNO_WAVE
